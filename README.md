## Assignment

Given an array of inputs (location name, postal code), log the current time and weather for those locations.

Example: "./weather New York, 10005, Tokyo, São Paulo, Pluto"

Follow our Code guidelines: https://github.com/invisible-tech/guidelines

You should use JavaScript, TypeScript is a plus.

The code should be self-documenting, although you can use comments to convey the reason for your design decisions.

Make sure you commit your progress in a sensible way, if you're doing TDD you should start with tests.
	
## Implementation notes
	
	1. The app uses openweathermap as data source
	
	2. Skipped ZIP codes (openweathermap does not have them) and made a simple search by city name
	
	3. API key hidden from git

	4. Two versions - functional and OO

		a. invtech_test.ts (functional: accepts array of cities as arguments on the command line)
	
		b. invtech_oo.ts (object oriented: accepts array of cities when instantiated)
	
## Contact	

info@clippersys.eu
	
## Version	
	
2020-05-27
