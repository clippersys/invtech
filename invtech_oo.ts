/**
*	@assignment	Given an array of inputs (location name, postal code), log the current time and weather for those locations.
*				Example: "./weather New York, 10005, Tokyo, São Paulo, Pluto"
*				Follow our Code guidelines: https://github.com/invisible-tech/guidelines
*				You should use JavaScript, TypeScript is a plus.
*				The code should be self-documenting, although you can use comments to convey the reason for your design decisions.
*				Make sure you commit your progress in a sensible way, if you're doing TDD you should start with tests.
*
*				implementation notes:
*
*				1. The app uses openweathermap as data source
*				2. Skipped ZIP codes (openweathermap does not have them) and made a simple search by city name
*				3. API key hidden from git
*				4. Takes an array of cities as argument to the constructor when instantiated
*
*	@author		info@clippersys.eu
*	@version	2020-05-27
*
*/

class WeatherInfo {

	constructor(cities = ['New York','Singapore','Delhi','Moscow','No such place']) {
		this.fs = require('fs')
		this.request = require('request')
		this.root = 'https://api.openweathermap.org/data/2.5/weather?'
		this.cities = cities
		this.appkey = this.fs.readFileSync('apikey.txt')
		this.locations = JSON.parse(this.fs.readFileSync('cities.json'))
	}

	_getCityId(name) {
		let ctr = this.locations.length
		while (ctr--) {
			if (this.locations[ctr].name == name) {
				return this.locations[ctr].id
			}
		}
		return false
	}

	_getCelcius(temp) {
		let kelvin = 273.1
		return Math.round((temp-kelvin)*10)/10
	}

	_getHumanReadbleTime(unix) {
		return new Date(unix*1000)
	}

	_getSimpleWeather(body) {
		let weather = 'Cloudiness: '+body.clouds.all+'%. '
			weather += 'Humidity: '+body.main.humidity+'. '
			weather += 'Wind speed & direction: '+body.wind.speed+' knots, '+body.wind.deg+'°. '
			weather += ' Temperature between ' + this._getCelcius(body.main.temp_min) + ' and ' + this._getCelcius(body.main.temp_max) + ' C°' 
			weather += ', feels like '+this._getCelcius(body.main.feels_like)+' C°.'
		return weather
	}

	show() {
		let cityId = 0,
			url = '',
			retreviedCity = '',
			result = '',
			cities = this.cities

		cities.map((city) => {
			cityId = this._getCityId(city)
			if (cityId) {
				url = this.root+'id='+cityId+'&appid='+this.appkey
				this.request(url, { json: true }, (err, res, body) => {
			  		if (err) { 
		//	  			result = err 
			  			result = 'Problem connecting to: '+url
			  		} else {
			  			retreviedCity = body.name // use the city name delivered by the API 
				  		result = '\n\nThe weather in '+retreviedCity+' at '+this._getHumanReadbleTime(body.dt)+': '+this._getSimpleWeather(body)
			  		} 
		  			console.log(result)
				})
			}
		})
	}

}


//const w = new WeatherInfo() 
const w = new WeatherInfo(['Denpasar','Alicante','Los Angeles']) 

w.show()
