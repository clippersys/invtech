/**
*	@assignment	Given an array of inputs (location name, postal code), log the current time and weather for those locations.
*				Example: "./weather New York, 10005, Tokyo, São Paulo, Pluto"
*				Follow our Code guidelines: https://github.com/invisible-tech/guidelines
*				You should use JavaScript, TypeScript is a plus.
*				The code should be self-documenting, although you can use comments to convey the reason for your design decisions.
*				Make sure you commit your progress in a sensible way, if you're doing TDD you should start with tests.
*
*				implementation notes:
*
*				1. The app uses openweathermap as data source
*				2. Skipped ZIP codes (openweathermap does not have them) and made a simple search by city name
*				3. API key hidden from git
*				4. Takes a list of cities on the command line as arguments
*
*	@author		info@clippersys.eu
*	@version	2020-05-27
*
*/

'use strict'

const request = require('request'),
	  fs = require('fs'),
	  appkey = fs.readFileSync('apikey.txt'),
	  locations = JSON.parse(fs.readFileSync('cities.json')),
	  root = 'https://api.openweathermap.org/data/2.5/weather?'

function getCityId(name) {
	let ctr = locations.length
	while (ctr--) {
		if (locations[ctr].name == name) {
			return locations[ctr].id
		}
	}
	return false
}

function getCelcius(temp) {
	let kelvin = 273.1
	return Math.round((temp-kelvin)*10)/10
}

function getHumanReadbleTime(unix) {
	return new Date(unix*1000)
}

function getSimpleWeather(body) {
	let weather = 'Cloudiness: '+body.clouds.all+'%. '
		weather += 'Humidity: '+body.main.humidity+'. '
		weather += 'Wind speed & direction: '+body.wind.speed+' knots, '+body.wind.deg+'°. '
		weather += ' Temperature between ' + getCelcius(body.main.temp_min) + ' and ' + getCelcius(body.main.temp_max) + ' C°' 
		weather += ', feels like '+getCelcius(body.main.feels_like)+' C°.'
	return weather
}

let cityId = 0,
	url = '',
	retreviedCity = '',
	result = '',
	cities = ['New York','Singapore','Delhi','Moscow','No such place'] // if no cities are given on the command line

// use cities from the command line - if they exist
if (process.argv.slice(2).length > 0) {
	cities = process.argv.slice(2)
}	

cities.map((city) => {
	cityId = getCityId(city)
	if (cityId) {
		url = root+'id='+cityId+'&appid='+appkey
		request(url, { json: true }, (err, res, body) => {
	  		if (err) { 
//	  			result = err 
	  			result = 'Problem connecting to: '+url
	  		} else {
	  			retreviedCity = body.name // use the city name delivered by the API 
		  		result = '\n\nThe weather in '+retreviedCity+' at '+getHumanReadbleTime(body.dt)+': '+getSimpleWeather(body)
	  		} 
  			console.log(result)
		})
	}
})
